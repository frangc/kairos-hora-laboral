package com.grid.core.horalaboral.application.usecases;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class HoraLaboralUseCase {

    private final PathProvider pathProvider;

    public Map<String, List<String>> read() {
        try {
            return new Gson().fromJson(Files.readString(Path.of(this.pathProvider.getPath())), Map.class);
        } catch (IOException e) {
            log.error("Error occurred while reading: ", e);
            return Collections.emptyMap();
        }
    }

    public Map<String, List<String>> update(final String id, List<String> holidaysDTO) {
        try {
            Map<String, List<String>> vacationsMap = new Gson().fromJson(Files.readString(Path.of(this.pathProvider.getPath())),
                    Map.class);
            if (vacationsMap.containsKey(id)) vacationsMap.put(id, holidaysDTO);
            final Path write = Files.write(Path.of(this.pathProvider.getPath()), new Gson().toJson(vacationsMap).getBytes(),
                    StandardOpenOption.TRUNCATE_EXISTING);
            return new Gson().fromJson(Files.readString(write), Map.class);
        } catch (IOException e) {
            log.error("Error occurred while updating: ", e);
            return Collections.emptyMap();
        }
    }
}
