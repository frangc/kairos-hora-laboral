package com.grid.core.horalaboral.application.usecases;

import org.springframework.stereotype.Component;

@Component
public class PathProvider {

  public static final String HOME_HORALABORAL_VACATIONS_JSON = "/home/HORALABORAL/vacations.json";

  public String getPath() {
    return HOME_HORALABORAL_VACATIONS_JSON;
  }

}
