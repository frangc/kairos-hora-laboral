package com.grid.core.horalaboral.infrastructure.controller;

import com.grid.core.horalaboral.application.usecases.HoraLaboralUseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/horalaboral")
@RequiredArgsConstructor
public class HoraLaboralController {

    private final HoraLaboralUseCase horaLaboralUseCase;

    @GetMapping
    public ResponseEntity<Map<String, List<String>>> read() {
        return ResponseEntity.accepted().body(this.horaLaboralUseCase.read());
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody @Valid List<String> holidaysDTO, @PathVariable("id")
            String id) {
        return ResponseEntity.accepted().body(this.horaLaboralUseCase.update(id, holidaysDTO));
    }

    @GetMapping(value = "/whois")
    public ResponseEntity<Map<String, String>> whoIs() {
        return ResponseEntity.ok(Map.of("Curro", "090533", "Fran", "309339", "Chabir", "943078", "Paco", "154042", "Mario", "477069"));
    }

}
