package com.grid.core.horalaboral.application.usecases;

import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HoraLaboralUseCaseTest {

    public static final String FRANK_ID = "309339";
    private HoraLaboralUseCase horaLaboralUseCase;

    private PathProvider pathProvider;

    @BeforeEach
    public void setUp() {
        this.pathProvider = mock(PathProvider.class);
        this.horaLaboralUseCase = new HoraLaboralUseCase(this.pathProvider);
    }

    @Test
    public void testRead() {
        final File file = new File(this.getClass().getClassLoader().getResource("vacations.json").getFile());
        final String absolutePath = file.getAbsolutePath();

        when(this.pathProvider.getPath()).thenReturn(absolutePath);

        final Map<String, List<String>> read = this.horaLaboralUseCase.read();

        assertThat(read.containsKey(FRANK_ID), is(Boolean.TRUE));
    }

    @Test
    void testUpdate() throws Exception {
        final File file = new File(Objects.requireNonNull(this.getClass().getClassLoader().getResource("vacations.json")).getFile());
        final String absolutePath = file.getAbsolutePath();

        final String vacationsJson = IOUtils.toString(this.getClass().getResourceAsStream("/vacations.json"), StandardCharsets.UTF_8);
        final Map<String, List<String>> holidaysDTO = new Gson().fromJson(vacationsJson, Map.class);
        List<String> holidaysFran = (holidaysDTO.get(FRANK_ID));
        String currentTime = String.valueOf(System.currentTimeMillis());
        holidaysDTO.get(FRANK_ID).add(currentTime);

        when(this.pathProvider.getPath()).thenReturn(absolutePath);

        final Map<String, List<String>> updatedHolidays = this.horaLaboralUseCase.update(FRANK_ID, holidaysFran);

        assertThat(updatedHolidays.get(FRANK_ID).contains(currentTime), is(Boolean.TRUE));
    }

    @Test
    void testNonUpdateWhenReceivingNewId() {
        final File file = new File(this.getClass().getClassLoader().getResource("vacations.json").getFile());
        final String absolutePath = file.getAbsolutePath();

        String newId = UUID.randomUUID().toString();
        String currentTime = String.valueOf(System.currentTimeMillis());

        when(this.pathProvider.getPath()).thenReturn(absolutePath);

        final Map<String, List<String>> updatedHolidays = this.horaLaboralUseCase.update(newId, List.of(currentTime));

        assertThat(updatedHolidays.containsKey(newId), is(Boolean.FALSE));
    }

}