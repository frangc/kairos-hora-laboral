package com.grid.core.horalaboral.infrastructure.controller;

import static org.mockito.Mockito.mock;

import com.grid.core.horalaboral.application.usecases.HoraLaboralUseCase;
import org.junit.jupiter.api.BeforeEach;

class HoraLaboralControllerTest {

  private HoraLaboralController horaLaboralController;

  private HoraLaboralUseCase horaLaboralUseCase;

  @BeforeEach
  public void setUp() {
    this.horaLaboralUseCase = mock(HoraLaboralUseCase.class);
    this.horaLaboralController = new HoraLaboralController(this.horaLaboralUseCase);
  }

}